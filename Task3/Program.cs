﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Point first;
            Point second;
            Point third;
            bool isExit = false;
            List<Triangle> triangleList = new List<Triangle>();

            while (!isExit)
            {
                Console.WriteLine("Enter tringle name\r\n");
                string triangleName = Console.ReadLine();
                while (!IsEnterCoordinateCorrect(out isExit, out first)) ;
                if (isExit) break;
                while (!IsEnterCoordinateCorrect(out isExit, out second)) ;
                if (isExit) break;
                while (!IsEnterCoordinateCorrect(out isExit, out third)) ;
                if (isExit) break;
                triangleList.Add(new Triangle(triangleName, first, second, third));
                Console.WriteLine("Print triangle list? y/n");
                if (Console.ReadLine().Equals("y", StringComparison.CurrentCulture))
                {
                    //Sort triangles beafore printing
                    triangleList.Sort();
                    //Print
                    Print(triangleList);
                    //CLear list after printing.
                    triangleList.Clear();

                }
            }
        }

        static bool IsEnterCoordinateCorrect(out bool isExit, out Point result)
        {
            string userData;
            try
            {
                Console.WriteLine("Enter triangle point coordinate.\r\n");
                userData = Console.ReadLine();
                if (userData.Equals("exit", StringComparison.CurrentCulture))
                    isExit = true;
                result = Point.Parse(userData);
                isExit = false;
                return true;
            }
            catch (FormatException fex)
            {
                Console.WriteLine("Wrong data format. Example:\r\n");
                //возможно скорректировать данные.
                isExit = false;
                result = new Point();
                return false;
            }
            catch (InvalidOperationException ioex)
            {
                Console.WriteLine("InvalidOperationException. Example: \r\n");
                isExit = false;
                result = new Point();
                return false;
            }
        }
        static void Print(List<Triangle> list)
        {
            Console.WriteLine("=================Triangle List=====================\r\n");
            foreach (var item in list)
                Console.WriteLine(item.ToString());
            Console.WriteLine("====================================================\r\n");
        }
    }
}
