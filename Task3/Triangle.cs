﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;


namespace Task3
{
    public class Triangle : IComparable<Triangle>
    {
        #region [Fields]

        #endregion // [Fields]

        #region [Properties]

        public string Name { get; set; }
        public Point First { get; set; }
        public Point Second { get; set; }
        public Point Third { get; set; }

        #endregion // [Properties]

        #region [Constructors]

        public Triangle(string name, Point first, Point second, Point third)
        {
            Name = name;
            First = first;
            Second = second;
            Third = third;
        }

        #endregion // [Constructors]

        #region [Methods]

        #region [Public]

        public override string ToString()
        {
            if (GetSquare() > 100)
                return String.Format(@"[{0}]:{1}.{2} m", Name, (int)GetSquare() / 100, Math.Round(GetSquare() % 100));
            return String.Format(@"[{0}]:{1} cm", Name, Math.Round(GetSquare()));
        }
        public int CompareTo(Triangle other)
        {
            if (GetSquare() < other.GetSquare())
                return 1;
            if (GetSquare() > other.GetSquare())
                return -1;
            return 0;
        }

        #endregion // [Public]

        #region [Private]

        private double GetSquare()
        {
            double p = (GetSideLenght(First, Second) + GetSideLenght(First, Third) + GetSideLenght(Third,Second)) / 2;
            return p * (p - GetSideLenght(First, Second)) + p * (p - GetSideLenght(First, Third)) + p * (p - GetSideLenght(Third, Second));
        }

        private double GetSideLenght(Point firstPoint, Point secondPoint)
        {
            double xofset = Math.Max(firstPoint.X, secondPoint.X) - Math.Min(firstPoint.X, secondPoint.X);
            double yofset = Math.Max(firstPoint.Y, secondPoint.Y) - Math.Min(firstPoint.Y, secondPoint.Y);
            return Math.Sqrt(xofset * xofset + yofset * yofset);
        }
        #endregion // [Private]

        #endregion // [Methods]
    }
}
