﻿// -----------------------------------------------------------------------
// <copyright file="Command.cs" company="SoftServe">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.ChessBoard.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represent command class
    /// </summary>
    public class Command
    {
        #region [Constructors]

        public Command(string name, params int[] parameters)
        {
            Name = name;
            Params = parameters;
        }
       

        #endregion //[Constructors]

        #region [Properties]

        /// <summary>
        /// Command name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Command parameters
        /// </summary>
        public int[] Params { get; set; }

        #endregion // [Properties]
    }
}
