﻿// -----------------------------------------------------------------------
// <copyright file="CalculateController.cs"  company="SoftServe">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.ChessBoard.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.ChessBoard.Abstractions;

    /// <summary>
    /// Represent Calculator controller
    /// </summary>
    public class CalculateController:ICalculateContract
    {
        public bool[,] CalculateChessBoard(int width, int height)
        {
            bool [,] resultArray = new bool [width,height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if ((i + j) % 2 == 0)
                        resultArray[i,j] = true;
                    else 
                        resultArray[i,j] = false;
                }
            }
            return resultArray;
        }

        public void Dispose()
        {
          
        }
    }
}
