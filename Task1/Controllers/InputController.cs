﻿// -----------------------------------------------------------------------
// <copyright file="InputController.cs"  company="SoftServe">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.ChessBoard.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using System.Resources;
    using System.Globalization;
    using SoftServe.ChessBoard.Abstractions;
    using SoftServe.ChessBoard.Models;

    /// <summary>
    /// Represent controller for eneter user data
    /// </summary>
    public class InputController : IInputContract
    {
        #region [Fields]
        private ResourceManager mResourceManager = default(ResourceManager);
        private readonly CultureInfo mCulture = new CultureInfo("en-US");
        #endregion // [Fields]

        #region [Properties]

        public bool IsExit
        {
            get;
            private set;
        }

        #endregion // [Properties]

        #region [Constructors]
        public InputController(CultureInfo userCulture)
        {
            if (userCulture != default(CultureInfo))
                mCulture = userCulture;
            mResourceManager = new ResourceManager(typeof(global::SoftServe.ChessBoard.Resources.ChessBoardResource));
        }
        #endregion //[Constructors]

        #region [Methods]

        public void PrintChessboard(bool[,] boardArray)
        {
            for (int i = 0; i < boardArray.GetLength(0); i++)
            {
                string temp = "";
                for (int j = 0; j < boardArray.GetLength(1); j++)
                {
                    if (boardArray[i, j])
                        temp += "*";
                    else
                        temp += " ";
                }
                Console.WriteLine(temp + "\r\n");
            }
        }

        public Models.Command GetCommand(string promt)
        {
            Console.Write(promt);
            string userCommand = Console.ReadLine();
            string[] args = userCommand.Split(new char[] { ';' });
            int width, height;

            if (int.TryParse(args[0], out width) && int.TryParse(args[1], out height))
            {
                return new Command("Calculate", width, height);
            }
            Command command = new Command(userCommand);
            IsExit = (string.Compare(command.Name, mResourceManager.GetString("ExitCommand", mCulture), true) == 0);
            return command;
        }

        public string GetVersion()
        {
            return mResourceManager.GetString("Version", mCulture);
        }

        public string GetHelp()
        {
            return mResourceManager.GetString("Help", mCulture);
        }

        public void Dispose()
        {

        }

        #endregion // [Methods]
    }
}
