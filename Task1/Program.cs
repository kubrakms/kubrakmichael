﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftServe.ChessBoard.Controllers;
using System.Threading;
using System.Resources;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var inputController = new InputController(Thread.CurrentThread.CurrentCulture))
            {
                var resManager = new ResourceManager(typeof(global::SoftServe.ChessBoard.Resources.ChessBoardResource));
                while (!inputController.IsExit)
                {
                    var command = inputController.GetCommand(resManager.GetString("Prompt", Thread.CurrentThread.CurrentCulture));
                    if (string.Compare(command.Name, "Calculate", true) == 0)
                    {
                        bool[,] result;
                        using (var calculateController = new CalculateController())
                        {
                            result = calculateController.CalculateChessBoard((int)command.Params[0], (int)command.Params[1]);
                        }
                        inputController.PrintChessboard(result);
                        continue;
                    }
                    Console.WriteLine(inputController.GetVersion());
                    Console.WriteLine(inputController.GetVersion());
                }
                Console.WriteLine(resManager.GetString("FinalMessage", Thread.CurrentThread.CurrentUICulture));
                Console.ReadKey();
            }
        }
    }
}
