﻿// -----------------------------------------------------------------------
// <copyright file="ICalculateContract.cs" company="SoftServe">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.ChessBoard.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represent Calculation contract
    /// </summary>
    public interface ICalculateContract:IDisposable
    {
        bool[,] CalculateChessBoard(int width, int height);
       
    }
}
