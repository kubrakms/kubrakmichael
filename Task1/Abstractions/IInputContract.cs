﻿// -----------------------------------------------------------------------
// <copyright file="IInputController.cs"  company="SoftServe">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.ChessBoard.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.ChessBoard.Models;


    /// <summary>
    /// represent input controller
    /// </summary>
    public interface IInputContract:IDisposable
    {
        /// <summary>
        /// Print board in console.
        /// </summary>
        /// <param name="boardArray"></param>
        void PrintChessboard(bool[,] boardArray);
        /// <summary>
        /// True if user enter the Exit comand.
        /// </summary>
        bool IsExit { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="promt"></param>
        /// <returns></returns>
        Command GetCommand(string promt);
        /// <summary>
        /// Method witch return program version
        /// </summary>
        /// <returns>return current version </returns>
        string GetVersion();
        /// <summary>
        /// Help message
        /// </summary>
        /// <returns>return help message</returns>
        string GetHelp();

    }
}
