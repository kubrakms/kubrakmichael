﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Zoo;

namespace ZooManager.Interfaces
{
    public interface ITechnicalWorker : IKeeper
    {
        void Cleen(Aviary aviary);
    }
}
