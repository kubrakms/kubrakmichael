﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZooManager.Interfaces
{
    public interface IWalk
    {
        void Walk();
    }
}
