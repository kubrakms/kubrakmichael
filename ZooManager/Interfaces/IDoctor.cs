﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZooManager.Interfaces
{
    public interface IDoctor : IKeeper
    {
        void Treat(Animals.AnimalBase animal);
        
    }
}
