﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using ZooManager.Zoo;
using System.IO;

namespace ZooManager.Serializers
{
    public class JSONSerializer : ISerializer
    {
        public void Serialize(Zoo.ZooClass zoo, string fileMame)
        {
            string serValue = JsonConvert.SerializeObject(zoo);
            using (StreamWriter sw = new StreamWriter(fileMame))
            {
                sw.Write(serValue);
            }
        }

        public Zoo.ZooClass Deserialize(string fileName)
        {
            string value;
            using (StreamReader sr = new StreamReader(fileName))
            {
                value = sr.ReadToEnd();
            }
            return (ZooClass)JsonConvert.DeserializeObject(value, typeof(ZooClass));
        }
    }
}
