﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ZooManager.Zoo;

namespace ZooManager.Serializers
{
    [Serializable]
    public class BinarySerializer : ISerializer
    {
        public void Serialize(Zoo.ZooClass zoo, string fileName)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, zoo);
            File.WriteAllBytes(fileName, ms.ToArray());
        }

        public Zoo.ZooClass Deserialize(string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            ZooClass z = (ZooClass)bf.Deserialize(new FileStream(fileName, FileMode.Open));
            return z;
        }
    }
}
