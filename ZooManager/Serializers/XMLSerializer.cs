﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace ZooManager.Serializers
{
    public class XMLSerializer : ISerializer
    {
        public void Serialize(Zoo.ZooClass zoo, string fileName)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Zoo.ZooClass));
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                xmlSerializer.Serialize(sw, zoo);
            }
        }

        public Zoo.ZooClass Deserialize(string fileName)
        {
            Zoo.ZooClass zoo;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Zoo.ZooClass));
            using (StreamReader sr = new StreamReader(fileName))
            {
                zoo = (Zoo.ZooClass)xmlSerializer.Deserialize(sr);
            }
            return zoo;
        }
    }
}
