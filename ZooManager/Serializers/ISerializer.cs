﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Zoo;

namespace ZooManager.Serializers
{
    public interface ISerializer
    {
        void Serialize(ZooClass zoo, string fileName);
        ZooClass Deserialize(string fileName);
    }
}
