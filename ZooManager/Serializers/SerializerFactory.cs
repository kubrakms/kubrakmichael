﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZooManager.Serializers
{
    public class SerializerFactory
    {
        public static ISerializer GetSerializer(Enums.Serializers serializerType)
        {
            switch (serializerType)
            {
                case Enums.Serializers.XMLSerializer:
                    {
                        return new XMLSerializer();
                    }
                case Enums.Serializers.JSONSerializer:
                    {
                        return new JSONSerializer();
                    }
                case Enums.Serializers.BinarySerializer:
                    {
                        return new BinarySerializer();
                    }
                default:
                    {
                        return new XMLSerializer();
                    }
            }
        }
    }
}
