﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;
using ZooManager.Interfaces;

namespace ZooManager.Animals
{
    public abstract class AnimalBase
    {
        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public AnimalBase(string nickName, double weight,Sex sex,TypeOfFood foodType, int age,int maxAge)
        {

        }

        #endregion // [Constructors]

        #region [Methods]

        #region [Public]

        #endregion // [Public]

        #region [Protected]

        //protected abstract void MoveDefault();
        protected abstract void Eat();
        protected abstract void Sleep();
        protected abstract void Reproduce();

        #endregion //[Protected]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        static int lastId { get; set; }
        public int Id { get; set; }
        public double Weight { get; set; }
        public string NickName { get; set; }
        public Sex Sex { get; set; }
        public TypeOfFood FoodType { get; set; }
        public int Age { get; set; }
        public int MaxAdge { get; set; }
        public abstract MaturityStage AgeState { get; }
        public bool IsHungy
        {
            get { return isHungy; }
            set
            {
                isHungy = value;
                if (value == true)
                    NeedFeeding(this, new EventArgs());
            }
        }
        public bool IsSick
        {
            get { return isSick; }
            set
            {
                isSick = value;
                if (value == true)
                    NeedTreatment(this, new EventArgs());
            }
        }

        #endregion // [Properties]

        #region [Fields]
        bool isHungy;
        bool isSick;
        #endregion // [Fields]

        #region [Events]
        public event EventHandler NeedTreatment;
        public event EventHandler NeedFeeding;
        #endregion // [Events]






    }
}
