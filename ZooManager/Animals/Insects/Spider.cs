﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;
using ZooManager.Interfaces;

namespace ZooManager.Animals.Insects
{
    class Spider : InsectBase, ICrawl
    {
        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Spider(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWings, bool isPredatory, int spyStrength)
            : base(nickName, weight, sex, foodType, age, maxAge,hasWings,isPredatory)
        {
            SpyStrength = spyStrength;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]

      

        #endregion // [Public]

        #region [Private]

       

        protected override void Eat()
        {
            throw new NotImplementedException();
        }

        protected override void Sleep()
        {
            throw new NotImplementedException();
        }

        protected override void Reproduce()
        {
            throw new NotImplementedException();
        }
        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        public int SpyStrength { get; set; }
        public override MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }
        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

        public void Crawl()
        {
            throw new NotImplementedException();
        }
    }
}
