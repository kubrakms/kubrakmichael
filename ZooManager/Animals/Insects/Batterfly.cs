﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;
using ZooManager.Interfaces;

namespace ZooManager.Animals.Insects
{
    class Batterfly : InsectBase,IFly,ICrawl
    {
        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Batterfly(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWings, bool isPredatory, double wingSize)
            : base(nickName, weight, sex, foodType, age, maxAge,hasWings,isPredatory)
        {
            WingSize = wingSize;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       

        #endregion // [Public]

        #region [Private]

       

        protected override void Eat()
        {
            throw new NotImplementedException();
        }

        protected override void Sleep()
        {
            throw new NotImplementedException();
        }

        protected override void Reproduce()
        {
            throw new NotImplementedException();
        }
        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        public double WingSize { get; set; }

        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]


      

        public override MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Crawl()
        {
            throw new NotImplementedException();
        }

        public void Fly()
        {
            throw new NotImplementedException();
        }
    }
}
