﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;

namespace ZooManager.Animals.Insects
{
    public abstract  class InsectBase : AnimalBase
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public InsectBase(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWings, bool isPredatory)
            :base(nickName,weight,sex,foodType,age,maxAge)
        {
            HasWings = hasWings;
            IsPredatory = isPredatory;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]

        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        private bool hasWings;

        public bool HasWings
        {
            get { return hasWings; }
            set { hasWings = value; }
        }

        private bool isPredatory;

        public bool IsPredatory
        {
            get { return isPredatory; }
            set { isPredatory = value; }
        }



        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

    }
}
