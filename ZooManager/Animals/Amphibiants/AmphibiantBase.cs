﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;

namespace ZooManager.Animals.Amphibiants
{
    public abstract class AmphibiantBase : AnimalBase
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public AmphibiantBase(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool isHaveLegs)
            :base(nickName,weight,sex,foodType,age,maxAge)
        {
            IsHaveLegs = isHaveLegs;
        }
       
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]

        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        private bool isHaveLegs;

        public bool IsHaveLegs
        {
            get { return isHaveLegs; }
            set { isHaveLegs = value; }
        }

        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]
       
    }
}
