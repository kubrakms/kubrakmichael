﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Amphibiants
{
    public class Chameleon : AmphibiantBase,IWalk,ISwim,IRun
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Chameleon(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool isHaveLegs, int colorsCount)
            : base(nickName, weight, sex, foodType, age, maxAge, isHaveLegs)
        {
            ColorsCount = colorsCount;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #region [Override]

      
        protected override void Eat()
        {
            
        }
        protected override void Sleep()
        {
            
        }
        protected override void Reproduce()
        {
            
        }

        #endregion // [Override]
        #endregion //  [Methods]

        #region [Properties]

        public int ColorsCount { get; set; }

        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

        public override Enums.MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
