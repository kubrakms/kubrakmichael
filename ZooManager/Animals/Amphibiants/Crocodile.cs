﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Amphibiants
{
    public class Crocodile : AmphibiantBase,  ISwim,IWalk,IRun
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Crocodile(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool isHaveLegs, double jawsPressure)
            : base(nickName, weight, sex, foodType, age, maxAge, isHaveLegs)
        {
            JawsPressure = jawsPressure;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #region [Override]

       
        protected override void Eat()
        {
            
        }
        protected override void Sleep()
        {
            
        }
        protected override void Reproduce()
        {
            
        }

        #endregion // [Override]
        #endregion //  [Methods]

        #region [Properties]

        public double JawsPressure
        {
            get;
            set;
        }

        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

        public override Enums.MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            throw new NotImplementedException();
        }
    }
}
