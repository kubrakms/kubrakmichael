﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;
using ZooManager.Interfaces;

namespace ZooManager.Animals.Amphibiants
{
    public class Anaconda : AmphibiantBase,ICrawl,ISwim
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]

        public Anaconda(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool isHaveLegs, double length)
            : base(nickName, weight, sex, foodType, age, maxAge, isHaveLegs)
        {
            Length = length;
        }

        #endregion // [Constructors]

        #region [Methods]

        #region [Public]



        #endregion // [Public]

        #region [Private]


        #endregion // [Private]

        #region [Override]

      
        protected override void Eat()
        {

        }
        protected override void Sleep()
        {

        }
        protected override void Reproduce()
        {

        }

        #endregion // [Override]

        #endregion //  [Methods]

        #region [Properties]

        public override Enums.MaturityStage AgeState
        {
            get { return MaturityStage.Newborn; }
        }

        public double Length { get; set; }
        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

        public void Crawl()
        {
            throw new NotImplementedException();
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }
    }
}
