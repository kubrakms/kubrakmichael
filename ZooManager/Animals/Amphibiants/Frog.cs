﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Amphibiants
{
    public class Frog : AmphibiantBase, ISwim,IWalk,IJump
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Frog(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool isHaveLegs, bool canChangeSex)
            : base(nickName, weight, sex, foodType, age, maxAge, isHaveLegs)
        {
            CanChangeSex = canChangeSex;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #region [Override]

      
        protected override void Eat()
        {
            
        }
        protected override void Sleep()
        {
            
        }
        protected override void Reproduce()
        {
            
        }

        #endregion // [Override]
        #endregion //  [Methods]

        #region [Properties]

        public bool CanChangeSex
        {
            get;
            set;
        }

        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

        public override Enums.MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Jump()
        {
            throw new NotImplementedException();
        }
    }
}
