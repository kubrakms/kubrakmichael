﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Artiodactyles
{
    public class Hog : ArtiodactylesBase,IWalk, IRun, ISwim
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Hog(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool, bool hasCanines)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool)
        {
            HasCanines = hasCanines;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]


        #endregion //  [Methods]

        #region [Properties]

        private bool hasCaninas;

        public bool HasCanines
        {
            get { return hasCaninas; }
            set { hasCaninas = value; }
        }

        private bool hasNickle;

        public bool HasNickle
        {
            get { return hasNickle; }
            set { hasNickle = value; }
        }

        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]


        public void Run()
        {
            throw new NotImplementedException();
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }
    }
}
