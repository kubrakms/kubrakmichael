﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Artiodactyles
{
    public class Hippopotamus : ArtiodactylesBase,IWalk,IRun,ISwim
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Hippopotamus(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool, int mouthSize)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool)
        {
            MouthSize = mouthSize;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
        
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]


        #endregion //  [Methods]

        #region [Properties]

        public int MouthSize { get; set; }

        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

        public void Run()
        {
            throw new NotImplementedException();
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }
    }
}
