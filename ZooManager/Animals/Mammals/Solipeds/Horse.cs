﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Solipeds
{
    public class Horse : SolipedBase,IWalk, IRun, ISwim,IJump
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Horse(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool, string breed)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool)
        {
            Breed = breed;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
     
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        private string breed;

        public string Breed
        {
            get { return breed; }
            set { breed = value; }
        }

        private string color;

        public string Color
        {
            get { return color; }
            set { color = value; }
        }



        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]


        public override Enums.MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            throw new NotImplementedException();
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }

        public void Jump()
        {
            throw new NotImplementedException();
        }
    }
}
