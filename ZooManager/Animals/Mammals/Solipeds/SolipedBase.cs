﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Solipeds
{
    public abstract class SolipedBase : MammalBase
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public SolipedBase(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool)
        {
            HoofsCount = 1;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       

       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #region [Override]

        
        protected override void Eat()
        {
            
        }
        protected override void Sleep()
        {
            
        }
        protected override void Reproduce()
        {
            
        }

        #endregion // [Override]

        #endregion //  [Methods]

        #region [Properties]
        public int HoofsCount { get; set; }
        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

    }
}
