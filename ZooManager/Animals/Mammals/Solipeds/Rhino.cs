﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Solipeds
{
    public class Rhino : SolipedBase, IWalk, IRun, ISwim
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Rhino(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool, int hornLengh)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool)
        {
            HornLengh = hornLengh;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]


        private double hornLengh;

        public double HornLengh
        {
            get { return hornLengh; }
            set { hornLengh = value; }
        }

        private string color;

        public string Color
        {
            get { return color; }
            set { color = value; }
        }


        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]


        public override Enums.MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            throw new NotImplementedException();
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }
    }
}
