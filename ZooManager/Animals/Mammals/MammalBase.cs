﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals
{
    public abstract class MammalBase : AnimalBase
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public MammalBase(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool)
            :base(nickName,weight,sex,foodType,age,maxAge)
        {
            HasWool = hasWool;
        }
        #endregion // [Constructors]

        #region [Methods]
         
        #region [Public]

        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        private bool hasWool;

        public bool HasWool
        {
            get { return hasWool; }
            set { hasWool = value; }
        }

        private bool isPakeAnimal;

        public bool IsPakeAnimal
        {
            get { return isPakeAnimal; }
            set { isPakeAnimal = value; }
        }


        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

    }
}
