﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;
using ZooManager.Interfaces;

namespace ZooManager.Animals.Mammals.Predators
{
    public class Tiger : PredatorBase,IRun,IWalk,ISwim
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Tiger(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool, bool isPredatory,string whoolColor,bool isLikeWater)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool, whoolColor)
        {
            IsLikeWater = isLikeWater;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        private bool isLikeWater;

        public bool IsLikeWater
        {
            get { return isLikeWater; }
            set { isLikeWater = value; }
        }
        
        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]


        public override MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }

        public void Run()
        {
            throw new NotImplementedException();
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public void Swim()
        {
            throw new NotImplementedException();
        }
    }
}
