﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Predators
{
    public abstract class PredatorBase : MammalBase
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public PredatorBase(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool,string whoolColor)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool)
        {
            WhoolColor = whoolColor;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
       
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #region [Override]

      
        protected override void Eat()
        {
            
        }
        protected override void Sleep()
        {
            
        }
        protected override void Reproduce()
        {
            
        }

        #endregion // [Override]
        #endregion //  [Methods]

        #region [Properties]

        private string whoolColor;

        public string WhoolColor
        {
            get { return whoolColor; }
            set { whoolColor = value; }
        }


        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]

    }
}
