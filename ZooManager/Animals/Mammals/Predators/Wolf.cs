﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Predators
{
    public class Wolf : PredatorBase, IRun, IWalk, ISwim
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Wolf(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool, bool isPredatory, string whoolColor, bool forestSanitar)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool, whoolColor)
        {
            ForestStanitar = forestSanitar;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
      
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

        #endregion //  [Methods]

        #region [Properties]

        private bool forestSanitar;

        public bool ForestStanitar
        {
            get { return forestSanitar; }
            set { forestSanitar = value; }
        }


        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]


        public void Swim()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            throw new NotImplementedException();
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public override Enums.MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }
    }
}
