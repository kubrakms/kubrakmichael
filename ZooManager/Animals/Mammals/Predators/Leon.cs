﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using ZooManager.Enums;

namespace ZooManager.Animals.Mammals.Predators
{
    public class Leon : PredatorBase, IRun, IWalk, ISwim
    {

        #region [Constants]

        #endregion  //[Constants]

        #region [Constructors]
        public Leon(string nickName, double weight, Sex sex, TypeOfFood foodType, int age, int maxAge, bool hasWool, bool isPredatory, string whoolColor, bool hasMane)
            : base(nickName, weight, sex, foodType, age, maxAge, hasWool, whoolColor)
        {
            HasMane = hasMane;
        }
        #endregion // [Constructors]

        #region [Methods]

        #region [Public]
      
        #endregion // [Public]

        #region [Private]

        #endregion // [Private]

      

        #endregion //  [Methods]

        #region [Properties]

        private bool hasMane;

        public bool HasMane
        {
            get { return hasMane; }
            set { hasMane = value; }
        }

        private double maneLengh;

        public double ManeLengh
        {
            get { return maneLengh; }
            set { maneLengh = value; }
        }


        #endregion // [Properties]

        #region [Fields]

        #endregion // [Fields]


        public void Swim()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            throw new NotImplementedException();
        }

        public void Walk()
        {
            throw new NotImplementedException();
        }

        public override Enums.MaturityStage AgeState
        {
            get { throw new NotImplementedException(); }
        }
    }
}
