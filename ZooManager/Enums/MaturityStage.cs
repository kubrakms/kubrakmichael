﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZooManager.Enums
{
    public enum MaturityStage
    {
        Newborn=4,
        Baby=3,
        Teenager=1,
        Adult=1,
        Old=0
    }
}
