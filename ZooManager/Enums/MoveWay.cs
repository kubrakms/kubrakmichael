﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZooManager.Enums
{
    [Flags]
    public enum MoveWay
    {
        Fly,
        Walk,
        Swim,
        Jump,
        Crawl
    }
}
