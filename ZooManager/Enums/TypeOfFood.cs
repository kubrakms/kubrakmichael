﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZooManager.Enums
{
    public enum TypeOfFood
    {
        Carnivorous,
        Herbivorous,
        Omnivorous
    }
}
