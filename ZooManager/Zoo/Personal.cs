﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Interfaces;
using System.Collections;

namespace ZooManager.Zoo
{

    [Serializable]
    public class Personal :CollectionBase, ICollection<IMan>
    {


        public Personal()
        {
            MansList = new List<IMan>();
        }

        #region [Properties]
        List<IMan> MansList { get; set; }
        #endregion //[Properties]

        public void Add(IMan item)
        {
            MansList.Add(item);
        }

      

        public bool Contains(IMan item)
        {
            return MansList.Contains(item);
        }

        public void CopyTo(IMan[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

       

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(IMan item)
        {
            if (!MansList.Contains(item))
                return false;
            MansList.Remove(item);
            return true;
        }

        public new IEnumerator<IMan> GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
