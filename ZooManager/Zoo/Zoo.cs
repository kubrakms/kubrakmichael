﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Animals;
using System.Collections;
using ZooManager.Interfaces;
using Newtonsoft.Json;

namespace ZooManager.Zoo
{
    [Serializable]
    public class ZooClass
    {

        #region [Constants]
        public const string FILE_NAME = "serializedData";
        #endregion // [Constants]

        #region [Singleton]

        private static readonly ZooClass instance = new ZooClass();

        public static ZooClass Instance
        {
            get { return instance != null ? instance : new ZooClass(); }
        }

        private ZooClass()
        {
            Personals = new Personal();
            AviaryList = new List<Aviary>();
        }

        #endregion  [Singleton]

        ~ZooClass()
        {

        }

        #region [Strategy]
        public void SetSerializationStrategy(Enums.Serializers concreteSerialization)
        {
            m_serializer = Serializers.SerializerFactory.GetSerializer(concreteSerialization);
        }

        public void Serialize(Zoo.ZooClass zoo, string fileName)
        {
            m_serializer.Serialize(zoo, fileName);
        }
        public ZooClass Deserialize(string fileName)
        {
            return m_serializer.Deserialize(fileName);
        }
        #endregion //[Strategy]

        public void OnNeedTreat(object sender, EventArgs e)
        {
            //We can chack sick type in eventArgs
            foreach (var item in Personals)
            {
                if (item is IDoctor)
                {
                    var doctor = item as IDoctor;
                    doctor.Treat(sender as AnimalBase);
                    break;
                }
            }
        }
        public void OnNeedFeed(object sender, EventArgs e)
        {
            foreach (var item in Personals)
            {
                if (item is IKeeper)
                {
                    var keeper = item as IKeeper;
                    keeper.Feed(sender as AnimalBase);
                    break;
                }
            }
        }
        public void OnNeedCleen(object sender, EventArgs e)
        {
            foreach (var item in Personals)
            {
                if (item is ITechnicalWorker)
                {
                    var technicalWorker = item as ITechnicalWorker;
                    technicalWorker.Cleen(sender as Aviary);
                    break;
                }
            }
        }

        public Personal Personals { get; set; }
        public List<Aviary> AviaryList { get; set; }
        public Serializers.ISerializer m_serializer;

       
    }
}
