﻿



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZooManager.Animals;
using System.Collections;
using System.IO;

namespace ZooManager.Zoo
{
    /// <summary>
    /// Represent class witch save collection of aviary
    /// </summary>
    [Serializable]
    public class Aviary : ICollection<AnimalBase>
    {
        #region [Constans]
        //max animal in cell default value if not set in constructor
        private const int MAX_ANIMALS_DEFAULT = 20;
        #endregion //[Constans]

        #region [Constructors]
        /// <summary>
        /// default constructors for aviary
        /// </summary>
        public Aviary() : this(MAX_ANIMALS_DEFAULT) { }
        /// <summary>
        /// paramentrezed constructor.Initialize animal list
        /// </summary>
        /// <param name="maxAnimals">max count of animal wich can be puch into Aviary</param>
        public Aviary(int maxAnimals)
        {
            

            MaxAnimals = maxAnimals;
            Animals = new List<AnimalBase>();
        }

        #endregion //[Constructors]

        #region [Methods]

        /// <summary>
        /// Get animal from Aviary. 
        /// </summary>
        /// <param name="animalId"> animal id </param>
        /// <returns></returns>
        public AnimalBase GetFromAviary(int animalId)
        {
            AnimalBase animal = null;
            if (Contains(animalId))
            {
                animal = Animals[animalId];
                Remove(animal);
            }
            return animal;
        }
        public bool CanAdd(AnimalBase animal)
        {
            //if (AviaryCompatibilityList.Contains(animal.GetType()))
            //    return true;
            return false;
        }
        public void AddAnimalToAviary(AnimalBase animal)
        {
            //recreate AviaryCompatibilityList
            Add(animal);
        }

        #endregion // [Methods]

        #region [Properties]

        int MaxAnimals { get; set; }
        List<AnimalBase> Animals { get; set; }

        public bool IsNeedCleen
        {
            get { return isNeedCleen; }
            set
            {
                isNeedCleen = value;
                if (value)
                {
                    var needCleen = NeedCleen;
                    if (needCleen != null)
                        needCleen(this, new EventArgs());
                }
            }
        }

        #endregion // [Properties]

        #region [Fields]
        private bool isNeedCleen;
        #endregion [Fields]

        public event EventHandler NeedCleen;

        #region [ICollection<AnimalBase> realization]

        public void Add(AnimalBase item)
        {
            item.NeedFeeding += ZooManager.Zoo.ZooClass.Instance.OnNeedFeed;
            item.NeedTreatment += ZooManager.Zoo.ZooClass.Instance.OnNeedTreat;
        }

        public bool Remove(AnimalBase item)
        {
            if (!this.Contains(item))
                return false;
            item.NeedFeeding -= ZooManager.Zoo.ZooClass.Instance.OnNeedFeed;
            item.NeedTreatment -= ZooManager.Zoo.ZooClass.Instance.OnNeedTreat;
            return true;
        }

        public void Clear()
        {
            Animals.Clear();
        }

        public bool Contains(AnimalBase item)
        {
            return Animals.Contains(item);
        }

        public bool Contains(int animalId)
        {
            foreach (var item in Animals)
            {
                if (item.Id == animalId)
                    return true;
            }
            return false;
        }

        public void CopyTo(AnimalBase[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return Animals.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public IEnumerator<AnimalBase> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        #endregion //[ICollection<AnimalBase> realization]
    }
}
