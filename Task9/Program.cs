﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            long userData;
            bool isMore;
            do
                Console.WriteLine("Enter a number. \r\n");
            while (!long.TryParse(Console.ReadLine(), out userData));
            do
                Console.WriteLine("isMore. \r\n");
            while (!bool.TryParse(Console.ReadLine(), out isMore));

            var result = GetNumberSmollerOrBigger(userData, isMore);
            foreach (var item in result)
            {
                Console.WriteLine(item.ToString()+"\t");
            }
            Console.WriteLine("\r\n");
            Console.ReadKey();
        }

        static List<int> GetNumberSmollerOrBigger(long n, bool isMore)
        {
            List<int> resultList = new List<int>();
            int number = (int)Math.Sqrt(n);
            if (isMore)
                for (int i = 1; i <10 ; i++)
                    resultList.Add(number+i);
            else
                 for (int i = 1; i <10 ; i++)
                    resultList.Add(number-i);
            return resultList;
        }
    }
}
