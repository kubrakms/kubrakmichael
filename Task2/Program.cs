﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isExit = false;
            while (!isExit)
            {
                double firstEvelopHeight = 0, firstEvelopWidth = 0, secondEvelopHeight = 0, secondEvelopWidth = 0;
                string consoleData;
                bool dataCorrect = false;
                Console.WriteLine("Enter first envelop data in format a;b where a=height, b=width");
                while (!dataCorrect)
                {
                    consoleData = Console.ReadLine();
                    if (consoleData == "exit") return;
                    if (consoleData.Split(new char[] { ';' }).Length == 2 &&
                        Double.TryParse(consoleData.Split(new char[] { ';' })[0].Replace('.', ','), out firstEvelopHeight) &&
                        Double.TryParse(consoleData.Split(new char[] { ';' })[1].Replace('.', ','), out firstEvelopWidth))
                        dataCorrect = true;
                    else
                        Console.WriteLine("Wrong data try again. Data format must be like 10,33;15,78");
                }
                dataCorrect = false;
                Console.WriteLine("Enter second envelop data in format c;d where c=height, d=width");
                while (!dataCorrect)
                {
                    consoleData = Console.ReadLine();
                    if (consoleData == "Exit") return;
                    if (consoleData.Split(new char[] { ';' }).Length == 2 &&
                        Double.TryParse(consoleData.Split(new char[] { ';' })[0].Replace('.', ','), out secondEvelopHeight) &&
                        Double.TryParse(consoleData.Split(new char[] { ';' })[0].Replace('.', ','), out secondEvelopWidth))
                        dataCorrect = true;
                    else
                        Console.WriteLine("Wrong data try again. Data format must be like 10,33;15,78");
                }
                if (firstEvelopHeight < secondEvelopHeight && firstEvelopWidth < secondEvelopWidth){
                    Console.WriteLine("You can put first envelop into second\r\n"); continue;
                }
                if (firstEvelopHeight > secondEvelopHeight && firstEvelopWidth > secondEvelopWidth){
                    Console.WriteLine("You can put second envelop into first\r\n"); continue;
                }
                Console.WriteLine("You can't put some envelop into another\r\n");
            }
        }
    }
}
