﻿// -----------------------------------------------------------------------
// <copyright file="CalculatorController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.FactorialFunction.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.FactorialFunction.Abstractions;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CalculatorController : ICalulatorContract
    {
        public void Dispose()
        {
           
        }

        public ulong GetFactorial(int value)
        {
            ulong result = 1;
            for (uint i = 1; i <= value; i++)
                result *= i;
            return result;
        }
    }
}
