﻿// -----------------------------------------------------------------------
// <copyright file="InputContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.FactorialFunction.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.FactorialFunction.Abstractions;
    using SoftServe.FactorialFunction.Models;
    using System.Resources;
    using System.Globalization;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class InputController : IInputContract
    {

        #region [Fields]
        private ResourceManager mResourceManager = default(ResourceManager);
        private readonly CultureInfo mCulture = new CultureInfo("en-US");
        #endregion // [Fields]

        #region [Properties]

        public bool IsExit
        {
            get;
            private set;
        }

        #endregion // [Properties]

        #region [Constructors]
        public InputController(CultureInfo userCulture)
        {
            if (userCulture != default(CultureInfo))
                mCulture = userCulture;
            mResourceManager = new ResourceManager(typeof(global::SoftServe.FactorialFunction.Resources.FactorialFunctionResource));
        }
        #endregion //[Constructors]

        #region [Methods]

        public void PrintData(ulong value)
        {
            Console.WriteLine(value + "\r\n");
        }

        public Models.Command GetCommand(string promt)
        {
            Console.Write(promt + "\r\n");
            string userCommand = Console.ReadLine();
            string[] args = userCommand.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            int value;
            if (int.TryParse(args[0], out value) && value < 30)
            {
                return new Command("Calculate", value);
            }
            Command command = new Command(userCommand);
            IsExit = (string.Compare(command.Name, mResourceManager.GetString("ExitCommand", mCulture), true) == 0);
            return command;
        }

        public string GetVersion()
        {
            return mResourceManager.GetString("Version", mCulture);
        }

        public string GetHelp()
        {
            return mResourceManager.GetString("Help", mCulture);
        }

        public void Dispose()
        {

        }

        #endregion // [Methods]


    }
}
