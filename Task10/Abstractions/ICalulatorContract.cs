﻿// -----------------------------------------------------------------------
// <copyright file="ICalulatorContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.FactorialFunction.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ICalulatorContract:IDisposable
    {
        ulong GetFactorial(int value);

    }
}
