﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using SoftServe.RecurciveFactorial.Controllers;
using System.Resources;

namespace SoftServe.RecurciveFactorial
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var inputController = new InputController(Thread.CurrentThread.CurrentCulture))
            {
                var resManager = new ResourceManager(typeof(global::SoftServe.RecurciveFactorial.Resources.FactorialFunctionResource));
                while (!inputController.IsExit)
                {
                    var command = inputController.GetCommand(resManager.GetString("Prompt", Thread.CurrentThread.CurrentCulture));
                    if (string.Compare(command.Name, "Calculate", true) == 0)
                    {
                        ulong result;
                        using (var calculateController = new CalculatorController())
                        {
                            result = calculateController.GetFactorial((ulong)command.Params[0]);
                        }
                        inputController.PrintData(result);
                        continue;
                    }
                    Console.WriteLine(inputController.GetHelp());
                    Console.WriteLine(inputController.GetVersion());
                }
                Console.WriteLine(resManager.GetString("BayMessage", Thread.CurrentThread.CurrentUICulture) + " \r\n");
                Console.ReadKey();
            }
        }
    }
}
