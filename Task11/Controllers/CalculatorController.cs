﻿// -----------------------------------------------------------------------
// <copyright file="CalculatorController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.RecurciveFactorial.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.RecurciveFactorial.Abstractions;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CalculatorController : ICalulatorContract
    {
        public void Dispose()
        {
           
        }

        public ulong GetFactorial(ulong value)
        {
            if (value == 1)
                return 1;
            else
                return value * GetFactorial(--value);
        }
        
    }
}
