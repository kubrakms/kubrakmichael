﻿// -----------------------------------------------------------------------
// <copyright file="IInputContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.RecurciveFactorial.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.RecurciveFactorial.Models;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IInputContract:IDisposable
    {
        /// <summary>
        /// Print value.
        /// </summary>
        /// <param name="boardArray"></param>
        void PrintData(ulong value);
        /// <summary>
        /// True if user enter the Exit comand.
        /// </summary>
        bool IsExit { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="promt"></param>
        /// <returns></returns>
        Command GetCommand(string promt);
        /// <summary>
        /// Method witch return program version
        /// </summary>
        /// <returns>return current version </returns>
        string GetVersion();
        /// <summary>
        /// Help message
        /// </summary>
        /// <returns>return help message</returns>
        string GetHelp();
    }
}
