﻿// -----------------------------------------------------------------------
// <copyright file="Command.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.RecurciveFactorial.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Command
    {

        #region [Constructors]

        public Command(string name, params ulong[] parameters)
        {
            Name = name;
            Params = parameters;
        }


        #endregion //[Constructors]

        #region [Properties]

        /// <summary>
        /// Command name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Command parameters
        /// </summary>
        public ulong[] Params { get; set; }

        #endregion // [Properties]
    }
}
