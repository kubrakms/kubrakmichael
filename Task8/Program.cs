﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            string number;


            Console.WriteLine("Enter number\r\n");
            number = Console.ReadLine();
            // string number = "1121";
            int digits = number.Length % 3 == 0 ? number.Length / 3 : number.Length / 3 + 1;
            int i = number.Length;

            DigitToNumberConverterBase d = new DigitToNumberConverterBase();
            Stack<string> triadsStack = new Stack<string>();
            int degr = 0;
            while (i > 0)
            {
                i -= 3;
                string temp = string.Empty;
                string tens = string.Empty;
                string before19 = string.Empty;
                string hundred = string.Empty;
                string degree = string.Empty;
                //logic
                if (i < 0)
                    temp = number.Substring(0, i + 3);
                else
                    temp = number.Substring(i, 3);
                before19 = d.GetBefore19((int.Parse(temp) % 100));
                if (before19 == string.Empty)
                    tens = d.GetAfter19((int.Parse(temp) % 100));
                hundred = d.GetHunderds((int.Parse(temp) / 100));
                degree = d.GetDegree(degr);
                triadsStack.Push(String.Format(@"{0} {1} {2} {3}", hundred, tens, before19, degree));
                degr++;
            }
            StringBuilder sb = new StringBuilder();
            while (triadsStack.Count != 0)
                sb.Append(triadsStack.Pop());
            Console.WriteLine(sb.ToString());
            Console.ReadKey();

        }
    }
}
