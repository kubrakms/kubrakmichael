﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task8
{
    class DigitToNumberConverterBase
    {
        Dictionary<int, string> Before19Dictionary;
        Dictionary<int, string> TensDictionary;
        Dictionary<int, string> DegreeDictionary;


        public DigitToNumberConverterBase()
        {
            Before19Dictionary = new Dictionary<int, string>()
            {
                 {1,"one "},{2,"two "},{3,"three "},{4,"four "},{5,"five "},{6,"six "},{7,"seven "},{8,"eight "},{9,"nine "},
                 {10,"ten "},{11,"eleven "},{12,"twelve "},{13,"thirteen "},{14,"forteen "},{15,"fifteen "},
                 {16,"sixteen "},{17,"seventeen "},{18,"eighteen "},{19,"nineteen "}
            };
            TensDictionary = new Dictionary<int, string>()
            {
                 {2,"twenty "},{3,"thirty "},{4,"forty "},{5,"fifty "},{6,"sixty "},{7,"seventy "},{8,"eighty "},{9,"ninety "}
            };
            DegreeDictionary = new Dictionary<int, string>()
            {
                 {1,"thousend "},{2,"milion "},{3,"bilion "}
            };
        }

        public string GetBefore19(int number)
        {
            if (!Before19Dictionary.Keys.Contains(number))
                return string.Empty;
            return Before19Dictionary[number];
        }

        public string GetAfter19(int number)
        {
            return string.Format(@"{0} {1}", GetTens(number / 10), GetBefore19(number % 10));
        }

        public string GetHunderds(int number)
        {
            string hundred = GetBefore19(number);
            if (hundred != string.Empty)
                hundred += " hundred ";
            return hundred;
        }
        private string GetTens(int number)
        {
            if (!TensDictionary.Keys.Contains(number))
                return string.Empty;
            return TensDictionary[number];
        }

        public string GetDegree(int number)
        {
            if (!DegreeDictionary.Keys.Contains(number))
                return string.Empty;
            return DegreeDictionary[number];
        }
    }
}
