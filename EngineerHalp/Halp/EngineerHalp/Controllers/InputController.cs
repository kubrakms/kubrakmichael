﻿// -----------------------------------------------------------------------
// <copyright file="InputController.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.EngineerHalp.Abstractions;
    using SoftServe.EngineerHalp.Models;
    using System.Resources;
    using System.Globalization;

    /// <summary>
    /// Implement logic of IInputContract
    /// </summary>
    public class InputController:IInputContract
    {
        public InputController(CultureInfo culture)
        {
            if (culture != default(CultureInfo))
                mCulture = culture;
        }

        public void Initialize()
        {
            mResManager = new ResourceManager(typeof(global::SoftServe.EngineerHalp.Resources.HalpResource));        
        }
        public string InputFolder
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string OutputFolder
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsDown
        {
            get;
            private set;
        }

        public Command GetCommand(string promt)
        {
            Console.Write(promt);
            var userCommand = Console.ReadLine();
            var command = new Command() { Name = userCommand };
            IsDown = (string.Compare(command.Name, mResManager.GetString("ExitName", mCulture), true) == 0);
            return command;
        }

        public event EventHandler OnInputFolderChanged;

        public geometry.Gm.Point GetPoint(string promt)
        {
            throw new NotImplementedException();
        }

        public void PrintPoint(geometry.Gm.Point point)
        {
            throw new NotImplementedException();
        }

        public void SaveFile(System.IO.Stream stream, string path)
        {
            throw new NotImplementedException();
        }

        public System.IO.Stream LoadFile(string path)
        {
            throw new NotImplementedException();
        }

        public string GetProgramHelp()
        {
            return mResManager.GetString("HelpString", mCulture);
        }

        public string GetVersion()
        {
            return mResManager.GetString("HelpString", mCulture);
        }

        public void Dispose()
        {
           // throw new NotImplementedException();
        }


        private readonly CultureInfo mCulture = new CultureInfo("en-US");

        private ResourceManager mResManager = default(ResourceManager);
    }
}
