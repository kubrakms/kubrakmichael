﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftServe.EngineerHalp.Controllers;
using System.Threading;
using System.Resources;
using System.Globalization;

namespace SoftServe.EngineerHalp
{
    class Program
    {
        static void Main(string[] args)
        {

            using (Proxies.ITransformProxy contr = LogicContainer.Instance.GetService<Proxies.ITransformProxy>())
            {
                var items = contr.ReadFormat("", Proxies.eFormatType.XML);
            }
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru-RU");
            using (var controller = new InputController(Thread.CurrentThread.CurrentUICulture))
            {
                (controller as InputController).Initialize();
                var resManager = new ResourceManager(typeof(global::SoftServe.EngineerHalp.Resources.HalpResource));
                Console.WriteLine(controller.GetProgramHelp());
                while (!controller.IsDown)
                {
                    var command = controller.GetCommand(resManager.GetString("PromtString", Thread.CurrentThread.CurrentUICulture));

                    if (string.Compare(command.Name, "Compute", true) == 0)
                    {
                        continue;
                    }

                    Console.WriteLine(controller.GetProgramHelp());
                }
                Console.WriteLine(resManager.GetString("FinalMessage", Thread.CurrentThread.CurrentUICulture));
                Console.ReadKey();
            }
        }
    }
}
