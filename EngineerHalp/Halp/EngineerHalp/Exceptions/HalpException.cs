﻿// -----------------------------------------------------------------------
// <copyright file="HalpException.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    public class HalpException : Exception
    {
        public HalpException() { }
        public HalpException(string message) : base(message) { }
        public HalpException(string message, Exception inner) : base(message, inner) { }
        protected HalpException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
