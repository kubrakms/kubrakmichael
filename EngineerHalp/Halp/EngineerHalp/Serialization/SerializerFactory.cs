﻿// -----------------------------------------------------------------------
// <copyright file="SerializerFactory.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.EngineerHalp.Proxies;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SerializerFactory
    {
        public static ISerializer GetSerializer(eFormatType serializerType)
        {
            switch (serializerType)
            {
                case  eFormatType.XML:
                    {
                        return new XMLSerializer();
                    }
                case eFormatType.JSON:
                    {
                        return new JSonSerializer();
                    }
                case eFormatType.Binary:
                    {
                        return new BinarySerializer();
                    }
                case eFormatType.CSV:
                    {
                        return new CsvSerializer();
                    }
                default:
                    {
                        return new XMLSerializer();
                    }
            }
        }
    }
}
