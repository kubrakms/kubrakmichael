﻿// -----------------------------------------------------------------------
// <copyright file="JSonSerializer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json;
    using System.IO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class JSonSerializer : ISerializer
    {
        public void Serialize(IEnumerable<Models.HalpFormatData> data, string fileName)
        {
            string serValue = JsonConvert.SerializeObject(data);
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                sw.Write(serValue);
            }
        }

        public IEnumerable<Models.HalpFormatData> Deserialize(string fileName)
        {
            string value;
            using (StreamReader sr = new StreamReader(fileName))
            {
                value = sr.ReadToEnd();
            }
            return (IEnumerable<Models.HalpFormatData>)JsonConvert.DeserializeObject(value, typeof(IEnumerable<Models.HalpFormatData>));
        }
    }
}
