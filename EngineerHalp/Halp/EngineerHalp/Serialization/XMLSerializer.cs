﻿// -----------------------------------------------------------------------
// <copyright file="XmlSerializer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Xml.Serialization;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class XMLSerializer : ISerializer
    {
        public void Serialize(IEnumerable<Models.HalpFormatData> data, string fileName)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(IEnumerable<Models.HalpFormatData>));
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                xmlSerializer.Serialize(sw, data);
            }
        }

        public IEnumerable<Models.HalpFormatData> Deserialize(string fileName)
        {
            IEnumerable<Models.HalpFormatData> halpDataList;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(IEnumerable<Models.HalpFormatData>));
            using (StreamReader sr = new StreamReader(fileName))
            {
                halpDataList = (IEnumerable<Models.HalpFormatData>)xmlSerializer.Deserialize(sr);
            }
            return halpDataList;
        }
    }
}
