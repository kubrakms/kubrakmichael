﻿// -----------------------------------------------------------------------
// <copyright file="ISerializer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ISerializer
    {
        void Serialize(IEnumerable<Models.HalpFormatData> data, string fileName);
        IEnumerable<Models.HalpFormatData> Deserialize(string fileName);

    }
}
