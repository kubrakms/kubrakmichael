﻿// -----------------------------------------------------------------------
// <copyright file="BinarySerializer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class BinarySerializer : ISerializer
    {
        public void Serialize(IEnumerable<Models.HalpFormatData> data, string fileName)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, data);
            File.WriteAllBytes(fileName, ms.ToArray());
        }

        public IEnumerable<Models.HalpFormatData> Deserialize(string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            IEnumerable<Models.HalpFormatData> halpDataList = (IEnumerable<Models.HalpFormatData>)bf.Deserialize(new FileStream(fileName, FileMode.Open));
            return halpDataList;
        }
    }
}
