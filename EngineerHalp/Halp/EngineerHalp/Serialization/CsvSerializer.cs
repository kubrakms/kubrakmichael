﻿// -----------------------------------------------------------------------
// <copyright file="CsvSerializer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Serialization
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CsvSerializer : ISerializer
    {
        public void Serialize(IEnumerable<Models.HalpFormatData> data, string fileName)
        {
            using (StreamWriter sw = File.AppendText(fileName))
            {
                foreach (var item in data)
                    sw.WriteLine(item.ToString());
            }
        }

        public IEnumerable<Models.HalpFormatData> Deserialize(string fileName)
        {
           List<Models.HalpFormatData> halpDataList = new List<Models.HalpFormatData>();
            string fileData= File.ReadAllText(fileName, Encoding.UTF8);
            foreach (var item in fileData.Split(new string[]{"\r","\n"},StringSplitOptions.RemoveEmptyEntries))
            {
                string[] stringData = item.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                Models.HalpFormatData halpFormatData = new Models.HalpFormatData()
                {
                     Id = Int32.Parse(stringData[0]),
                     Name = stringData[1],
                     V1 = new geometry.Gm.Point(double.Parse(stringData[2]),double.Parse(stringData[3]),double.Parse(stringData[4])),
                     V2 = new geometry.Gm.Point(double.Parse(stringData[5]),double.Parse(stringData[6]),double.Parse(stringData[7])),
                     V3 = new geometry.Gm.Point(double.Parse(stringData[8]),double.Parse(stringData[9]),double.Parse(stringData[10]))
                };
                halpDataList.Add(halpFormatData);
            }
            return halpDataList;
        }
    }
}
