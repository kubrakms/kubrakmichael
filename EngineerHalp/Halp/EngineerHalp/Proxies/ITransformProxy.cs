﻿// -----------------------------------------------------------------------
// <copyright file="ITransformProxy.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Proxies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ITransformProxy:IDisposable
    {
        IEnumerable<Models.HalpFormatData> ReadFormat(string sourceFile, eFormatType typeSource);

        void WriteFormat(IEnumerable<Models.HalpFormatData> data, eFormatType typeDest, string destFile);
    }
}
