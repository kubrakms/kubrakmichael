﻿// -----------------------------------------------------------------------
// <copyright file="Moqs.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Proxies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Moq;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class Moqs
    {
        static Moqs()
        {
            try
            {
                Initalize();
            }
            catch (Exception)
            {
                /// TODO: Add log audit logic here
            }
        }

        public static ITransformProxy TransformProxyMoq { get { return mTransformProxyMoq.Object; } }
        public static IFormatProxy FormatProxyMoq { get { return mFormatProxyMoq.Object; } }

        private static void Initalize()
        {
            lock (__mutex)
            {
                initializeFormat();

                initializeTransform();
            }
        }

        private static void initializeTransform()
        {
            mTransformProxyMoq.Setup(x => x.ReadFormat(It.IsAny<String>(), It.IsAny<eFormatType>()))
                .Returns<string, eFormatType>((fileSource, format) =>
                {
                    return new Models.HalpFormatData[] { };
                });
            mTransformProxyMoq.Setup(x => x.Dispose()).Callback(() =>
            {
                Console.WriteLine("Disposed!");
            });
        }

        private static void initializeFormat()
        {

        }

        private static readonly object __mutex = new object();
        private static readonly Mock<IFormatProxy> mFormatProxyMoq = new Mock<IFormatProxy>(MockBehavior.Loose);
        private static readonly Mock<ITransformProxy> mTransformProxyMoq = new Mock<ITransformProxy>(MockBehavior.Strict);
    }
}
