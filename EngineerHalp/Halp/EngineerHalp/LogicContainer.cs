﻿// -----------------------------------------------------------------------
// <copyright file="LogicContainer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.EngineerHalp.Proxies;
    using Autofac;
    using AutoMapper;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class LogicContainer :IServiceProvider
    {
        private LogicContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance<IFormatProxy>(Moqs.FormatProxyMoq);
            builder.RegisterInstance<ITransformProxy>(Moqs.TransformProxyMoq);

            m_Container = builder.Build();
        }

        #region Nested private logic

        private class TypedParameterTypeConverter : ITypeConverter<object, TypedParameter>
        {
            public TypedParameter Convert(ResolutionContext context)
            {
                //if (context.SourceValue is IPathResolver)
                //    return new TypedParameter(typeof(IPathResolver), context.SourceValue);

                //if (context.SourceValue is IDistributedRepository)
                //    return new TypedParameter(typeof(IDistributedRepository), context.SourceValue);

                return new TypedParameter(context.SourceType, context.SourceValue);
            }
        }

        #endregion

        #region IServiceProvider implementation

        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(IFormatProxy))
                return m_Container.Resolve<IFormatProxy>();

            if (serviceType == typeof(ITransformProxy))
                return m_Container.Resolve<ITransformProxy>();

            return null;
        }

        #endregion

        #region Thread safe singletone

        public static LogicContainer Instance
        {
            get
            {
                lock (__mutex)
                {
                    if (m_Instance == null)
                        m_Instance = new LogicContainer();
                }
                return m_Instance;
            }
        }

        #endregion

        public T GetService<T>()
        {
            return m_Container.Resolve<T>();
        }

        public T GetService<T>(object[] parameters)
        {
            if (parameters == null)
                return GetService<T>();

            var @params = Mapper.Map<object[], IEnumerable<TypedParameter>>(parameters);
            return m_Container.Resolve<T>(@params);
        }

        private static readonly object __mutex = new object();
        private static LogicContainer m_Instance = default(LogicContainer);

        private readonly IContainer m_Container = default(IContainer);
    }
}
