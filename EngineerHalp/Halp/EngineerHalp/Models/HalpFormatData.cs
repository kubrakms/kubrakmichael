﻿// -----------------------------------------------------------------------
// <copyright file="HalpFormatData.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;
    using geometry;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    [Serializable]
    [DataContract]
    public class HalpFormatData
    {
        [DataMember(Order = 1)]
        public long Id { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public Gm.Point V1 { get; set; }

        [DataMember(Order = 4)]
        public Gm.Point V2 { get; set; }

        [DataMember(Order = 5)]
        public Gm.Point V3 { get; set; }

        public override string ToString()
        {
           return String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}",
                    Id.ToString(),
                    Name,
                    V1.x.ToString(),
                    V1.y.ToString(),
                    V1.z.ToString(),
                    V2.x.ToString(),
                    V2.y.ToString(),
                    V2.z.ToString(),
                    V3.x.ToString(),
                    V3.y.ToString(),
                    V3.z.ToString());
        }
    }
}
