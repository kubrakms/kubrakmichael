﻿// -----------------------------------------------------------------------
// <copyright file="Command.cs" company="">
//  general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represent commands struct
    /// </summary>
    public class Command
    {
        /// <summary>
        /// TODO:
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// TODO:
        /// </summary>
        public object[] Params { get; set; }

    }
}
