﻿// -----------------------------------------------------------------------
// <copyright file="IInputContract.cs" company="">
// general rule - no rules
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using geometry;
    using System.IO;
    using SoftServe.EngineerHalp.Models;

    /// <summary>
    /// Represent input logic
    /// </summary>
    public interface IInputContract:IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        string InputFolder { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string OutputFolder { get; set; }
        /// <summary>
        /// True if user enter the Exit comand.
        /// </summary>
        bool IsDown { get; }

        event EventHandler OnInputFolderChanged;

        /// <summary>
        /// Provides input Point model functionality
        /// </summary>
        /// <param name="promt">Help string for user</param>
        /// <returns>
        /// <value>Point is entered by user</value>
        /// <value>Default(Point) if input was failed</value>
        /// </returns>
        Gm.Point GetPoint(string promt);

        /// <summary>
        /// Print point
        /// </summary>
        /// <param name="point"></param>
        /// <exception cref="ArgumentException">When Point is ivalid</exception>
        void PrintPoint(Gm.Point point);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="promt"></param>
        /// <returns></returns>
        Command GetCommand(string promt);

        /// <summary>
        /// Saves file to disc
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="path">File path</param>
        /// <exception cref="ArgumentException">When path is not avaialable</exception>
        /// <exception cref="ArgumentNullException">When path or stream is nallable</exception>
        void SaveFile(Stream stream, string path);

        /// <summary>
        /// Load file from disk
        /// </summary>
        /// <param name="path"></param>
        /// <returns> return stram of open file</returns>
        /// <exception cref="ArgumentException">When path is not avaialable</exception>
        Stream LoadFile(string path);
        /// <summary>
        /// Get help for avaliable comand
        /// </summary>
        /// <returns> return list of avaliable comands</returns>
        string GetProgramHelp();
        /// <summary>
        /// Get current version of application
        /// </summary>
        /// <returns>Current version of application</returns>
        string GetVersion();
    }
}
